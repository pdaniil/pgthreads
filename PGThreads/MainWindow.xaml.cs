﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PGThreads
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private Thread[] threads;
        private int threadCount = 5;
        private bool stopWork = false;

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (threads != null)
            {
                MessageBox.Show("Threads already running");
                return;
            }
            stopWork = false;
            threads = new Thread[threadCount];
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(new ParameterizedThreadStart(doWork));
                threads[i].IsBackground = true;
                threads[i].Start(i);
            }
        }

        private void doWork(object x)
        {
            int i = 0;
            var rand = new Random();
            while (!stopWork)
            {
                i++;
                string text = string.Format("[{0}] {1} {2}\n", (int)x, i, DateTime.Now);
                Dispatcher.Invoke(() =>
                {
                    textBox.Text += text;
                });
                Thread.Sleep(rand.Next(100, 1000) * ((int)x + 1));
            }
        }

        private void Abort_Click(object sender, RoutedEventArgs e)
        {
            if (threads == null)
            {
                MessageBox.Show("Nothing to abort");
                return;
            }
            foreach (var thread in threads)
                thread.Abort(); // убьет поток
            threads = null;
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            if (threads == null)
            {
                MessageBox.Show("Nothing to stop");
                return;
            }
            stopWork = true;
            foreach (var thread in threads)
                thread.Join();// будет ждать пока поток не завершится
            threads = null;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (threads != null)
                Abort_Click(this, null);
        }
    }
}
